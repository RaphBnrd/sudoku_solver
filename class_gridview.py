"""
This files contains the class GridView
It's the interface and the function that oper a
strategy on solving the Grid
"""

import tkinter as tk
import numpy as np
from functools import partial
import copy

from class_grid import Grid


class GridView(tk.Tk):
    def __init__(self, grid=None, cell_size=40,
                 size=9, possible_values=[1, 2, 3, 4, 5, 6, 7, 8, 9],
                 max_iterations=30):
        tk.Tk.__init__(self)
        # We define the grid
        if grid != None:
            self.grid = grid
        else:
            self.grid = Grid(size=size, possible_values=possible_values,
                             grid_matrix=[[None]*size]*size, 
                             max_iterations=max_iterations)
        self.cell_size = cell_size
        self.geometry(str(self.cell_size*self.grid.size+20 + 450) + 'x' + \
                      str(self.cell_size*self.grid.size+20))
        self.selected_cell = (1,1) # (row, column)
        self.max_iterations = max_iterations
        self.fill_draft = False
        self.tried_grids = {'0':self.grid}
        self.grid_key = '0'
        # Initial display before the computations
        self.display_canv()
        for i in self.grid.possible_values:
            self.bind(str(i), self.init_canv_valKey)
        self.bind('<Delete>', self.init_canv_valKey)
        self.bind('<space>', self.switch_draft_spaceKey)
        self.launch_button = tk.Button(self, text ="Lancer la résolution",
                                       font=('Helvetica', 10, 'bold'),
                                       command = self.launch_computations)
        self.launch_button.place(x = self.cell_size*self.grid.size+20 + 50,
                                 y = 300)

    
    def display_canv(self):
        # Canvas of the grid
        self.canv_grid = tk.Canvas(self, height=self.cell_size*self.grid.size,
                                   width=self.cell_size*self.grid.size,
                                   bg='white', highlightthickness=3, 
                                   highlightbackground="black")
        self.canv_grid.place(x=10 , y=10)
        # Build lines
        for k in range(1, self.grid.size):
            if k%(self.grid.size**(1/2)) == 0:
                line_width = 3
            else:
                line_width = 1
            self.canv_grid.create_line(k*self.cell_size, 0,
                                       k*self.cell_size, self.cell_size*self.grid.size, 
                                       fill='black', width=line_width)
            self.canv_grid.create_line(0, k*self.cell_size,
                                       self.cell_size*self.grid.size, k*self.cell_size, 
                                       fill='black', width=line_width)
        # Fill cells
        for k in range(self.grid.size):
            for l in range(self.grid.size):
                cell = self.grid.grid[k][l]
                if cell.initial == True:
                    text_color = 'black'
                else:
                    text_color = 'blue'
                draft_color = '#626262'
                # Value
                if cell.value != None:
                    self.canv_grid.create_text(int((l+1/2)*self.cell_size),
                                            int((k+1/2)*self.cell_size),
                                            text=str(cell.value),
                                            fill=text_color, anchor=tk.CENTER,
                                            font=("Helvetica", 15, 'bold'))
                # Possible values
                else:
                    for pos in cell.possible_values:
                        self.canv_grid.create_text(int( ( l + (1/2 + (pos-1) %(int(self.grid.size**(1/2)))) / int(self.grid.size**(1/2)) ) * self.cell_size ),
                                                   int( ( k + (1/2 + (pos-1)//(int(self.grid.size**(1/2)))) / int(self.grid.size**(1/2)) ) * self.cell_size ),
                                                   text=str(pos),
                                                   fill=draft_color, anchor=tk.CENTER,
                                                   font=("Helvetica", 8))
        # Grid selected
        grid_selected = tk.Label(text='Grid '+self.grid_key, anchor=tk.W,
                                 justify=tk.LEFT, font=("Helvetica", 17))
        grid_selected.place(x = self.cell_size*self.grid.size+20 + 20,
                            y = 20) 
        # Selected cell
        self.rect_select = self.canv_grid.create_rectangle((self.selected_cell[1]-1)*self.cell_size,
                                                           (self.selected_cell[0]-1)*self.cell_size,
                                                           self.selected_cell[1]   *self.cell_size,
                                                           self.selected_cell[0]   *self.cell_size,
                                                           outline='#FFC300', width=2)
        # Option for the selected cell
        self.canv_grid.bind("<Button 1>",self.canv_click)
        self.bind('<Left>', self.canv_leftKey)
        self.bind('<Right>', self.canv_rightKey)
        self.bind('<Up>', self.canv_upKey)
        self.bind('<Down>', self.canv_downKey)
        # Infos on the selected cell
        text_cell = "Cellule n°"+str(self.selected_cell)
        cell = self.grid.grid[self.selected_cell[0]-1][self.selected_cell[1]-1]
        if cell.value == None:
            text_cell += "\n\nValeurs possibles : \n"+str(cell.possible_values)
        else:
            text_cell += "\n\nValeur : "+str(cell.value)
        self.label_cell = tk.Label(text=text_cell,
                                   anchor=tk.W,
                                   justify=tk.LEFT,
                                   font=("Helvetica", 17))
        self.label_cell.place(x = self.cell_size*self.grid.size+20 + 20,
                              y = 70)
        # Label for writing mode
        if self.fill_draft == True:
            text_mode = "Mode d'écriture : \nBrouillon"
        else:
            text_mode = "Mode d'écriture : \nNormal"
        self.label_mode = tk.Label(text=text_mode,
                                   anchor=tk.W,
                                   justify=tk.LEFT,
                                   font=("Helvetica", 12))
        self.label_mode.place(x = self.cell_size*self.grid.size+20 + 20,
                              y = 200)
        # Possible grids
        for k in range(len(self.tried_grids)):
            key = list(self.tried_grids.keys())[k]
            button = tk.Button(self, text=key, command=partial(self.change_pos_grid, key))
            button.place(x = self.cell_size*self.grid.size+20 + 260 + 40*(k%4),
                         y = 50 + 30*(k//4))


    def destroy_and_display_canv(self):
        self.canv_grid.destroy()
        self.label_cell.destroy()
        self.display_canv()

    def change_pos_grid(self, key):
        self.grid = self.tried_grids[key]
        self.grid_key = key
        self.destroy_and_display_canv()


    def update_selected(self):
        self.canv_grid.moveto(self.rect_select,
                              (self.selected_cell[1]-1)*self.cell_size,
                              (self.selected_cell[0]-1)*self.cell_size)
        text_cell = "Cellule n°"+str(self.selected_cell)
        cell = self.grid.grid[self.selected_cell[0]-1][self.selected_cell[1]-1]
        if cell.value == None:
            text_cell += "\n\nValeurs possibles : \n"+str(cell.possible_values)
        else:
            text_cell += "\n\nValeur : "+str(cell.value)
        self.label_cell['text'] = text_cell

    def canv_click(self, event):
        self.selected_cell = (event.y//self.cell_size+1, event.x//self.cell_size+1)
        self.update_selected()
    def canv_leftKey(self, event):
        self.selected_cell = (self.selected_cell[0], max(1, self.selected_cell[1]-1))
        self.update_selected()
    def canv_rightKey(self, event):
        self.selected_cell = (self.selected_cell[0], min(self.grid.size, self.selected_cell[1]+1))
        self.update_selected()
    def canv_upKey(self, event):
        self.selected_cell = (max(1, self.selected_cell[0]-1), self.selected_cell[1])
        self.update_selected()
    def canv_downKey(self, event):
        self.selected_cell = (min(self.grid.size, self.selected_cell[0]+1), self.selected_cell[1])
        self.update_selected()
    def switch_draft_spaceKey(self, event):
        self.fill_draft = not(self.fill_draft)
        if self.fill_draft == True:
            text_mode = "Mode d'écriture : \nBrouillon"
        else:
            text_mode = "Mode d'écriture : \nNormal"
        self.label_mode['text'] = text_mode

    def init_canv_valKey(self, event):
        cell = self.grid.grid[self.selected_cell[0]-1][self.selected_cell[1]-1]
        if self.fill_draft == False:
            if event.keysym == 'Delete':
                cell.value = None
                cell.possible_values = self.grid.possible_values
                cell.initial = False
                print("Valeur supprimée dans la case",self.selected_cell)
            else:
                cell.value = int(event.keysym)
                cell.possible_values = [cell.value]
                cell.initial = True
                print("Valeur  \"",event.keysym,"\"  dans la case",self.selected_cell)
        else:
            if event.keysym != 'Delete':
                if int(event.keysym) in cell.possible_values:
                    cell.possible_values.remove(int(event.keysym))
                else:
                    cell.possible_values.append(int(event.keysym))
                    cell.possible_values.sort()
        self.destroy_and_display_canv()


    def launch_computations(self):
        self.grid.init_fill_all_possible()
        print("\n\n--- La résolution commence ---")
        bool_iter, count = self.grid.try_to_resolve()
        print("Quelque chose a bougé à la dernière itération ?     ", bool_iter,
            "\nNombre d'itérations :                               ", count)
        # If finished
        if bool_iter == False and self.grid.finished_resolution():
            print("\n--- La résolution est terminée ---")
            print(repr(np.array([[cell.value for cell in self.grid.grid[k]] \
                                             for k in range(self.grid.size)])))
        # If blocked
        elif bool_iter == False and self.grid.check_grid():
            print("\nLa résolution n'avance plus...")
            print(repr(np.array([[cell.value for cell in self.grid.grid[k]] \
                                             for k in range(self.grid.size)])))
            print("\nOn va faire des hypothèses : ")
            (idxs, hyp_vals) = self.search_where_hyp()
            for k in range(len(hyp_vals)):
                hyp_grid = self.build_hyp_grid(idxs, hyp_vals[k])
                new_key = self.grid_key+'.'+str(k)
                self.tried_grids[new_key] = hyp_grid
        # If false
        elif bool_iter == False:
            print("\nIl y a une erreur...")
        # If not finished but not blocked
        else:
            print("\nIl faut encore itérer pour résoudre")
            print(repr(np.array([[cell.value for cell in self.grid.grid[k]] \
                                             for k in range(self.grid.size)])))
        self.destroy_and_display_canv()
    

    def search_where_hyp(self):
        # pass
        nbr_pos = self.grid.size + 1
        chosen_cell = None
        for i in range(self.grid.size):
            for j in range(self.grid.size):
                if self.grid.grid[i][j].value == None:
                    if len(self.grid.grid[i][j].possible_values) < nbr_pos:
                        chosen_cell = self.grid.grid[i][j]
                        nbr_pos = len(self.grid.grid[i][j].possible_values)
        print("Cellule choisie pour faire une hypothèse :     (" + \
              str(chosen_cell.idx_row+1)+","+str(chosen_cell.idx_col+1)+")")
        print("Choix faits pour cette cellule :    ", chosen_cell.possible_values)
        return ((chosen_cell.idx_row,chosen_cell.idx_col), chosen_cell.possible_values)


    def build_hyp_grid(self, idxs, hyp_val):
        grid_matrix = [[self.grid.grid[k][l].value for l in range(self.grid.size)] \
                                                   for k in range(self.grid.size)]
        hyp_grid = Grid(size=self.grid.size, possible_values=copy.copy(self.grid.possible_values), 
                        grid_matrix=grid_matrix, max_iterations=self.grid.max_iterations)
        hyp_grid.grid[idxs[0]][idxs[1]].value = hyp_val
        hyp_grid.grid[idxs[0]][idxs[1]].possible_values = [hyp_val]
        return hyp_grid



if __name__ == "__main__":
    
    
    ###### EXEMPLE 1 #######
    
    view0 = GridView()
    view0.mainloop()



    ####### EXEMPLE 2 #######

    # ini_grid = [[None, None,   1 ,    None,   4 , None,    None, None,   3 ],
    #             [None, None, None,    None,   8 , None,    None, None, None],
    #             [None, None, None,    None,   9 ,   1 ,    None,   7 , None],

    #             [None,   6 ,   9 ,    None, None, None,      2 , None, None],
    #             [None, None, None,    None, None,   2 ,    None, None, None],
    #             [  4 , None,   7 ,      6 ,   1 , None,      5 , None, None],
                         
    #             [None, None, None,    None, None, None,    None, None, None],
    #             [  8 , None, None,    None,   7 , None,      9 ,   5 , None],
    #             [  9 , None, None,      3 , None, None,    None,   6 , None]]
    # view0 = GridView(grid=Grid(grid_matrix=ini_grid))
    # view0.mainloop()



    ####### EXEMPLE 3 #######

    # ini_grid = [[None, None, None,    None, None, None,      4 , None,   9 ],
    #             [  2 , None, None,    None,   1 , None,    None, None, None],
    #             [None, None, None,    None, None, None,      6 , None,   1 ],

    #             [  3 , None, None,      8 , None,   1 ,    None, None, None],
    #             [  8 , None, None,      6 , None,   9 ,    None, None, None],
    #             [None,   7 , None,    None, None, None,    None,   1 , None],
                         
    #             [  7 ,   9 ,   8 ,    None, None, None,    None, None, None],
    #             [  1 , None, None,    None,   5 , None,      9 ,   7 , None],
    #             [  4 ,   5 ,   6 ,    None, None, None,    None, None, None]]
    # view0 = GridView(grid=Grid(grid_matrix=ini_grid))
    # view0.mainloop()



    ####### EXEMPLE 4 #######

    # ini_grid = [[  6 , None,   8 ,    None, None, None,    None, None,   1 ],
    #             [None, None, None,    None,   2 , None,      5 , None, None],
    #             [None, None, None,    None, None, None,    None, None, None],

    #             [None, None, None,      8 , None,   9 ,    None,   3 , None],
    #             [  7 ,   5 , None,    None, None, None,    None, None, None],
    #             [None,   2 , None,    None, None, None,    None, None, None],
                
    #             [  4 , None, None,      6 ,   5 , None,    None, None, None],
    #             [None, None, None,      1 , None, None,    None,   8 , None],
    #             [None, None, None,    None, None, None,      9 , None, None]]
    # view0 = GridView(grid=Grid(grid_matrix=ini_grid))
    # view0.mainloop()
    