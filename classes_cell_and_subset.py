"""
This files contains the classes Cell and Subset
with the subclasses Row, Column and Square

Subset and Square contains the elementary method to solve
the sudoku
"""

from itertools import combinations


class Cell(object):
    def __init__(self, idx_row=0, idx_col=0, value=None,
                 possible_values=[1, 2, 3, 4, 5, 6, 7, 8, 9],
                 row=None, column=None, square=None,
                 initial=False):
        self.idx_row = idx_row
        self.idx_col = idx_col
        self.value = value
        self.possible_values = possible_values
        self.row = row
        self.column = column
        self.square = square
        self.initial = initial

class Subset(object):
    def __init__(self, idx=0, cells=[]):
        self.idx = idx
        self.cells = cells
    
    def constraint_subset_direct(self):
        something_append = False
        for cell in self.cells:
            if cell.value != None:
                for other_cell in self.cells:
                    if other_cell != cell:
                        if cell.value in other_cell.possible_values:
                            other_cell.possible_values.remove(cell.value)
                            something_append = True
        return something_append
    
    def constraint_subset_complexe(self):
        something_append = False
        # Get all possible values in the subset
        list_all_possibles = []
        idx_unknown_cell = []
        for k in range(len(self.cells)):
            if self.cells[k].value == None:
                idx_unknown_cell.append(k)
                list_all_possibles += self.cells[k].possible_values
        set_all_possibles = set(list_all_possibles)
        nbr_val = len(set_all_possibles)

        for k in range(1, nbr_val):
            all_subsets_k_val = list(combinations(set_all_possibles, k))
            all_subsets_k_idx_cell = list(combinations(set(idx_unknown_cell), k))
            for subset_val in all_subsets_k_val:
                for subset_idx_cells in all_subsets_k_idx_cell:

                    # Are the values of these cells only in the subset of values ?
                    is_only_in_these_val = True
                    # (We browse these cell to check it)
                    for idx in subset_idx_cells:
                        for val in self.cells[idx].possible_values:
                            if val not in subset_val:
                                is_only_in_these_val = False
                    # If yes, we remove these values from the rest of the cells
                    if is_only_in_these_val == True:
                        for idx in idx_unknown_cell:
                            if idx not in subset_idx_cells:
                                for val in subset_val:
                                    if val in self.cells[idx].possible_values:
                                        self.cells[idx].possible_values.remove(val)
                                        something_append = True

                    # Are these cells, the only to have these values in it ?
                    is_the_only_one_with_it = True
                    # (We browse the other cells to check it)
                    for idx in idx_unknown_cell:
                        if idx not in subset_idx_cells:
                            for val in subset_val:
                                if val in self.cells[idx].possible_values:
                                    is_the_only_one_with_it = False
                    # If yes, we remove the other values in this cell
                    if is_the_only_one_with_it == True:
                        for val in set_all_possibles:
                            if val not in subset_val:
                                for idx in subset_idx_cells:
                                    if val in self.cells[idx].possible_values:
                                        self.cells[idx].possible_values.remove(val)
                                        something_append = True
        return something_append

    def fill_unic_posval_subset(self):
        something_append = False
        for cell in self.cells:
            if cell.value == None:
                for posval in cell.possible_values:
                    is_somewhere_else = False
                    for other_cell in self.cells:
                        if other_cell != cell:
                            if posval in other_cell.possible_values:
                                is_somewhere_else = True
                    if is_somewhere_else == False:
                        cell.values = posval
                        cell.possible_values = [posval]
                        something_append = True
        return something_append
    

    def check_subset(self, possible_values):
        subset_ok = True
        for val in possible_values:
            count = 0
            for cell in self.cells:
                if cell.value == val:
                    count += 1
            if count > 1:
                subset_ok = False
        return subset_ok




class Row(Subset):
    def __init__(self, idx=0, cells=[]):
        self.idx = idx
        self.cells = cells

class Column(Subset):
    def __init__(self, idx=0, cells=[]):
        self.idx = idx
        self.cells = cells

class Square(Subset):
    def __init__(self, idx=0, cells=[]):
        self.idx = idx
        self.cells = cells

    def constraint_subsets_crossed(self, line):
        something_append = False
        # Find the intersection
        intersect = []
        for cell_squ in self.cells:
            for cell_line in line.cells:
                if cell_squ == cell_line:
                    intersect.append(cell_squ)
        # Find the possible values in this intersection
        list_all_possibles = []
        for cell in intersect:
            if cell.value == None:
                list_all_possibles += cell.possible_values
        set_all_possibles = set(list_all_possibles)
        for val in set_all_possibles:

            # Does this value appear somewhere else in the square?
            somewhere_else_square = False
            for cell_squ in self.cells:
                if cell_squ not in intersect:
                    if val in cell_squ.possible_values:
                        somewhere_else_square = True
            # If not, we remove it from the rest of the line
            if somewhere_else_square == False:
                for cell_line in line.cells:
                    if cell_line not in intersect:
                        if val in cell_line.possible_values:
                            cell_line.possible_values.remove(val)
                            something_append = True

            # Does this value appear somewhere else in the line?
            somewhere_else_line = False
            for cell_line in line.cells:
                if cell_line not in intersect:
                    if val in cell_line.possible_values:
                        somewhere_else_line = True
            # If not, we remove it from the rest of the square
            if somewhere_else_line == False:
                for cell_squ in self.cells:
                    if cell_squ not in intersect:
                        if val in cell_squ.possible_values:
                            cell_squ.possible_values.remove(val)
                            something_append = True

        return something_append
