"""
This files contains the class Grid
It's the structure of the sudooku
This class contains the global resolution functions 
"""

from classes_cell_and_subset import Cell, Row, Column, Square


class Grid(object):
    def __init__(self, size=9, possible_values=[1, 2, 3, 4, 5, 6, 7, 8, 9],
                 grid_matrix=[[None]*9]*9, max_iterations=30):
        if size**(1/2) != int(size**(1/2)):
            print("Warning: Size of a grid has to be an square number")
        self.size = size
        self.possible_values = possible_values
        self.max_iterations = max_iterations
        # Build a list of list of None
        self.grid = []
        for k in range(self.size):
            self.grid.append([])
            for l in range(self.size):
                self.grid[k].append(None)
        # Build rows, columns and squares
        self.rows = []
        self.columns = []
        self.squares = []
        for k in range(size):
            self.rows.append(Row(idx=k, cells=[]))
            self.columns.append(Column(idx=k, cells=[]))
            self.squares.append(Square(idx=k, cells=[]))
        # Build cells
        if len(grid_matrix)==size and len(grid_matrix[0])==size:
            for k in range(len(grid_matrix)):
                for l in range(len(grid_matrix[0])):
                    if grid_matrix[k][l] == None:
                        this_possible_values = possible_values[:]
                        initial = False
                    else:
                        this_possible_values = [grid_matrix[k][l]]
                        initial = True
                    num_row, num_col = k, l
                    num_squ = int(l//(int(size**(1/2))) + int(size**(1/2))*(k//(int(size**(1/2)))))
                    self.grid[k][l] = Cell(idx_row=k, 
                                           idx_col=l, 
                                           value=grid_matrix[k][l],
                                           possible_values=this_possible_values,
                                           row=self.rows[num_row],
                                           column=self.columns[num_col],
                                           square=self.squares[num_squ],
                                           initial=initial)
                    self.rows[num_row].cells.append(self.grid[k][l])
                    self.columns[num_col].cells.append(self.grid[k][l])
                    self.squares[num_squ].cells.append(self.grid[k][l])
    
    def all_subset_constraints_direct(self):
        something_append = False
        for k in range(self.size):
            something_append_row = self.rows[k].constraint_subset_direct()
            something_append_col = self.columns[k].constraint_subset_direct()
            something_append_squ = self.squares[k].constraint_subset_direct()
            if something_append_row or something_append_col or something_append_squ:
                something_append = True
        return something_append
    

    def all_subset_constraints_complexe(self):
        something_append = False
        for k in range(self.size):
            something_append_row = self.rows[k].constraint_subset_complexe()
            something_append_col = self.columns[k].constraint_subset_complexe()
            something_append_squ = self.squares[k].constraint_subset_complexe()
            if something_append_row or something_append_col or something_append_squ:
                something_append = True
        return something_append
    

    def all_crossed_subset_constraints(self):
        something_append = False
        for k in range(self.size):
            for l in range(self.size):
                something_append_row = self.squares[k].constraint_subsets_crossed(self.rows[l])
                something_append_col = self.squares[k].constraint_subsets_crossed(self.columns[l])
                if something_append_row or something_append_col:
                    something_append = True
        return something_append


    def fill_unic_posval_all_subset(self):
        something_append = False
        for k in range(self.size):
            something_append_row = self.rows[k].fill_unic_posval_subset()
            something_append_col = self.columns[k].fill_unic_posval_subset()
            something_append_squ = self.squares[k].fill_unic_posval_subset()
            if something_append_row or something_append_col or something_append_squ:
                something_append = True
        return something_append


    def fill_unic_posval_cell(self):
        something_append = False
        for k in range(self.size):
            for l in range(self.size):
                cell = self.grid[k][l]
                if cell.value==None and len(cell.possible_values)==1:
                    cell.value = cell.possible_values[0]
                    something_append = True
        return something_append
    

    def check_grid(self):
        grid_ok = True
        errors = {"row":[], "col":[], "squ":[]}
        for k in range(self.size):
            row_ok = self.rows[k].check_subset(self.possible_values)
            col_ok = self.columns[k].check_subset(self.possible_values)
            squ_ok = self.squares[k].check_subset(self.possible_values)
            if not(row_ok):
                grid_ok = False
                errors["row"].append(k+1)
            if not(col_ok):
                grid_ok = False
                errors["col"].append(k+1)
            if not(squ_ok):
                grid_ok = False
                errors["squ"].append(k+1)
        if grid_ok:
            print("\n\nLa grille ne semble pas avoir d'erreur")
        else:
            print("\n\n /!\   LA GRILLE A UNE ERREUR   /!\ ")
            if len(errors["row"]) > 0:
                print("Ligne(s)   :      ", errors["row"])
            if len(errors["col"]) > 0:
                print("Colonne(s) :      ", errors["col"])
            if len(errors["squ"]) > 0:
                print("Carré(s)   :      ", errors["squ"])
        return grid_ok
    
    def finished_resolution(self):
        if not(self.check_grid()):
            return False
        else:
            for k in range(self.size):
                for l in range(self.size):
                    if self.grid[k][l].value == None:
                        print("La grille est incomplète")
                        return False
        return True
    

    def try_to_resolve(self):
        bool_iter = True
        count = 0
        while bool_iter==True and count<self.max_iterations:
            print("\n\nSTEP", count)
            bool1_iter = self.fill_unic_posval_cell()
            bool2_iter = self.fill_unic_posval_all_subset()
            bool3_iter = self.all_subset_constraints_direct()
            bool4_iter = self.all_subset_constraints_complexe()
            bool5_iter = self.all_crossed_subset_constraints()
            bool_iter = bool1_iter or bool2_iter or bool3_iter or bool4_iter or bool5_iter
            count += 1
        return bool_iter, count
    

    def init_fill_all_possible(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.grid[i][j].value == None:
                    self.grid[i][j].possible_values = self.possible_values[:]
